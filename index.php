<?php
$arr = [
    1, 2, 3
];

$arrTwo = [
    'Hello', 'World',
];

$arrThree = [
    'a' => 'Hello',
    'b' => 'World',
];

$arrFoo = [
    'h' => [
        'w' => 'Hello World'
    ]
];

print $arr[0] . ", " . $arr[1] . ", " . $arr[2] . "\n";
print $arrTwo[0] . " " . $arrTwo[1] . "\n";
print $arrThree['b'] . ", " . $arrThree['a'] . "\n";
print $arrFoo['h']['w'] . "\n";

print_r($arrFoo);
